package com.mujadid.spkpentablet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.flexbox.FlexboxLayout;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.activities.TabletActivity;
import com.mujadid.spkpentablet.models.Tablet;
import com.mujadid.spkpentablet.utils.ViewUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TabletAdapter extends RecyclerView.Adapter<TabletAdapter.ViewHolder> {

    // Global variable
    private final Context context;
    private ArrayList<Tablet> pentablets;

    // The rest of the code
    public TabletAdapter(Context context) {
        this.pentablets = new ArrayList<Tablet>();
        this.context = context;
    }

    public void setData(List<Tablet> pentablets) {
        this.pentablets = new ArrayList<>(pentablets);
        notifyDataSetChanged();
    }

    // mengembalikan size dari dataset
    @Override
    public int getItemCount() {
        return pentablets.size();
    }

    @Override
    public @NonNull
    ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_tablet, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        // Inisialisasi value yang akan di bind
        Tablet current = pentablets.get(position);
        String type = current.type;
        String fireScore = new DecimalFormat("0.00").format(current.fireScore);
        String title = current.brand + " " + current.model;
        String pressurelv = current.pressureLevels + " levels";
        String workingArea = current.workingAreaDiagonalInch + " inch";
        String price = "$ " + new DecimalFormat("###,###.00").format(current.price);
        String priceIDR = "Rp " + new DecimalFormat("###,###").format(current.priceIDR);


        // bind value
        viewHolder.price.setText(price);
        viewHolder.priceIDR.setText(priceIDR);
        viewHolder.title.setText(title);

        viewHolder.descriptions.removeAllViews();
        viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_tablet_landscape_fill_24, type));
        viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_aspect_ratio_fill_24, workingArea));
        viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_brush_fill_24, pressurelv));

        if (current.totalKeys > 0) {
            String totalKeysStr = String.format("%d total keys", current.totalKeys);
            viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_command_24, totalKeysStr));
        }
        if (current.radialKey > 0) {
            viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_radial_fill_24, "Radial/wheel/ring key"));
        }

        if (current.tiltRecognitionSupport > 0) {
            viewHolder.descriptions.addView(createDescription(R.drawable.ic_baseline_tilt_fill_24, "Tilt recognition support"));
        }

        viewHolder.image.setAlpha(1f);

        // muat gambar
        Glide.with(context)
                .load(current.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ImageViewCompat.setImageTintList(viewHolder.image, null);
                        return false;
                    }
                })
                .placeholder(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .error(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .into(viewHolder.image);

        // fungsi on click
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TabletActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("tablet", current);
                context.startActivity(intent);
            }
        });
    }

    public View createDescription(@DrawableRes int drawableRes, String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_icon_text, null);
        ((ImageView) view.findViewById(R.id.icon)).setImageDrawable(AppCompatResources.getDrawable(context, drawableRes));
        ((TextView) view.findViewById(R.id.text)).setText(text);
        return view;
    }

    // Holder yang akan digunakan
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final CardView cardView;
        public final TextView title;
        public final TextView fireScore;
        public final TextView price;
        public final TextView priceIDR;
        public final ImageView image;
        public final LinearLayout descriptions;

        public ViewHolder(View view) {
            super(view);
            descriptions = view.findViewById(R.id.descriptions);
            cardView = view.findViewById(R.id.card);
            title = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            priceIDR = view.findViewById(R.id.priceIDR);
            image = view.findViewById(R.id.image);
            fireScore = view.findViewById(R.id.score);
        }
    }

}


