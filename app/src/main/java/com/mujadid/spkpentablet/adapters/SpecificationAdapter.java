package com.mujadid.spkpentablet.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mujadid.spkpentablet.R;

import java.util.ArrayList;

public class SpecificationAdapter extends RecyclerView.Adapter<SpecificationAdapter.ViewHolder> {

    private ArrayList<String> names;
    private ArrayList<String> values;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView header;
        public final TextView name;
        public final TextView value;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.header);
            name = view.findViewById(R.id.name);
            value = view.findViewById(R.id.value);
        }
    }

    public SpecificationAdapter() {
        this.names = new ArrayList<>();
        this.values = new ArrayList<>();
    }

    public void addItem(String name, String value) {
        names.add(name);
        values.add(value);
        notifyItemInserted(values.size());
    }


    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_specification, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.name.setText(names.get(position));
        viewHolder.value.setText(values.get(position));
    }

}
