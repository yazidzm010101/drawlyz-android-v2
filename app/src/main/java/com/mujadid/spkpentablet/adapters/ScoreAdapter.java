package com.mujadid.spkpentablet.adapters;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.models.Tablet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.graphics.Typeface.BOLD;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {

    private ArrayList<Tablet.Criteria> criterias;
    private float firescore;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView criteria;
        public final TextView category;
        public final TextView score;
        public final TextView firescore;
        public final LinearLayout scoreContainer;

        public ViewHolder(View view) {
            super(view);
            criteria = view.findViewById(R.id.criteria);
            category = view.findViewById(R.id.category);
            score = view.findViewById(R.id.score);
            firescore = view.findViewById(R.id.firescore);
            scoreContainer = view.findViewById(R.id.score_container);
        }
    }

    public ScoreAdapter() {
        this.criterias = new ArrayList<>();
    }

    public void setData(List<Tablet.Criteria> criterias, float firescore) {
        this.criterias = new ArrayList<>(criterias);
        this.firescore = firescore;
    }


    @Override
    public int getItemCount() {
        return criterias.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_score, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        DecimalFormat df = new DecimalFormat("0.00");
        Tablet.Criteria current = criterias.get(position);
        SpannableStringBuilder categories = new SpannableStringBuilder("");
        int length = current.categories.size();

        for (int i = 0; i < length; i ++) {
            if (i > 1 || i > 0 && length > 2) {
                categories.append("; ");
            }
            if ( i == length - 1 && length > 1) {
                categories.append(" or ");
            }
            categories.append(current.categories.get(i).name);
            if (current.categories.get(i).score == current.groupScore) {
                int end = categories.length();
                int start = end - current.categories.get(i).name.length();
                categories.setSpan(new StyleSpan(BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        viewHolder.criteria.setText(current.name);
        viewHolder.category.setText(categories);
        viewHolder.score.setText(df.format(current.groupScore));

        if (position == criterias.size() - 1) {
            viewHolder.scoreContainer.setVisibility(View.VISIBLE);
            viewHolder.firescore.setText(df.format(firescore));
        }
    }

}
