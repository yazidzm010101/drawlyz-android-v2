package com.mujadid.spkpentablet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.models.Search;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    // Global variable
    private final Context context;
    private ArrayList<Search> searches;

    // The rest of the code
    public SearchAdapter(Context context) {
        this.searches = new ArrayList<>();
        this.context = context;
    }

    public void setData(List<Search> searches) {
        this.searches = new ArrayList<>(searches);
        notifyDataSetChanged();
    }

    // mengembalikan size dari dataset
    @Override
    public int getItemCount() {
        return searches.size();
    }

    @Override
    public @NonNull
    ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_search_container, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        // Inisialisasi value yang akan di bind
        Search current = searches.get(position);
        String noResult = String.format(context.getString(R.string.info_noresult), current.name);

        viewHolder.icon.setContentDescription(current.name);
        viewHolder.no_result.setText(noResult);

        if (current.data != null) {
            if (current.data.size() > 0) {
                viewHolder.data.setVisibility(View.VISIBLE);
                viewHolder.no_result.setVisibility(View.GONE);
            }
            for (int i = 0; i < current.data.size(); i++) {
                viewHolder.data.addView(createResult(current.data.get(i)));
            }
        }

        viewHolder.open_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = current.url;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(Intent.createChooser(browserIntent, "Choose browser"));
            }
        });

        // muat gambar
        Glide.with(context)
                .load(current.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ImageViewCompat.setImageTintList(viewHolder.icon, null);
                        return false;
                    }
                })
                .placeholder(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .error(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .into(viewHolder.icon);

    }

    public View createResult(Search.Result result) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search, null);
        CardView card = (CardView) view.findViewById(R.id.card);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView price = (TextView) view.findViewById(R.id.price);
        TextView priceLocal = (TextView) view.findViewById(R.id.priceLocal);
        title.setText(result.name);
        price.setText(result.price);
        priceLocal.setVisibility(View.GONE);

        Glide.with(context)
                .load(result.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ImageViewCompat.setImageTintList(image, null);
                        return false;
                    }
                })
                .placeholder(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .error(R.drawable.ic_baseline_tablet_landscape_fill_24)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .into(image);

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = result.url;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(Intent.createChooser(browserIntent, "Choose browser"));
            }
        });

        return view;
    }

    // Holder yang akan digunakan
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView icon;
        public final LinearLayout data;
        public final TextView no_result;
        public final FloatingActionButton open_web;

        public ViewHolder(View view) {
            super(view);
            open_web = view.findViewById(R.id.open_web);
            icon = view.findViewById(R.id.icon);
            data = view.findViewById(R.id.data);
            no_result = view.findViewById(R.id.no_result);
        }
    }

}


