package com.mujadid.spkpentablet.adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.IntroductionFragment;

public class IntroductionAdapter extends FragmentPagerAdapter {
    Context context;

    public IntroductionAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        int resId = position == 0 ? R.layout.introduction_about
                : position == 1 ? R.layout.introduction_explore
                : position == 2 ? R.layout.introduction_favorite
                : position == 3 ? R.layout.introduction_fuzzy
                : 0;
        if (resId != 0) {
            return IntroductionFragment.newInstance(resId);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

}
