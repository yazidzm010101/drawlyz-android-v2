package com.mujadid.spkpentablet.adapters;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.models.Criteria;

import java.util.ArrayList;
import java.util.List;

public class CriteriaAdapter extends RecyclerView.Adapter<CriteriaAdapter.ViewHolder> {

    // Global variable
    private ArrayList<Criteria> criterias;
    private ArrayList<ChipGroup> chipGroups;
    private Context context;

    // Holder yang akan digunakan
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ChipGroup chipGroup;
        public final TextView header;
        public final FrameLayout divider;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.header);
            chipGroup = view.findViewById(R.id.criteria);
            divider = view.findViewById(R.id.divider);
        }
    }

    public CriteriaAdapter(Context context) {
        this.context = context;
        chipGroups = new ArrayList<>();
        criterias = new ArrayList<>();
    }

    public void setData(List<Criteria> criterias) {
        this.criterias = new ArrayList<>(criterias);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_criteria, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Criteria current = criterias.get(position);
        viewHolder.header.setText(current.name);

        ChipGroup chipGroup = viewHolder.chipGroup;
        for (int i = 0; i < current.categories.size(); i++) {
            Chip chip = new Chip(new ContextThemeWrapper(context, R.style.Widget_MaterialComponents_Chip_Choice));
            chip.setCheckable(true);
            chip.setText(current.categories.get(i));
            chipGroup.addView(chip);
        }
        chipGroups.add(chipGroup);

        if (position == getItemCount() - 1) {
            viewHolder.divider.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return criterias.size();
    }

    public ArrayList<Criteria> getSelectedCriterias() {
        ArrayList<Criteria> criterias = new ArrayList<>();
        for (int i = 0; i < this.chipGroups.size(); i++) {
            List<Integer> ids = chipGroups.get(i).getCheckedChipIds();
            if (ids.size() > 0) {
                ArrayList<String> categories = new ArrayList<>();
                for (int j = 0; j < ids.size(); j++) {
                    String category = ((Chip) chipGroups.get(i).findViewById(ids.get(j))).getText().toString();
                    categories.add(category);
                }
                Criteria criteria = new Criteria();
                criteria.name = this.criterias.get(i).name;
                criteria.categories = categories;
                criterias.add(criteria);

            }
        }
        return criterias;
    }

}
