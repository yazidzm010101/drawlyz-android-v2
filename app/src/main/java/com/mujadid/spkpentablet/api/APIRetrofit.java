package com.mujadid.spkpentablet.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIRetrofit {
    private static Retrofit mainRetrofit;
    private static Retrofit puppeteerRetrofit;

    public static Retrofit getMainRetrofit() {
        String BASE_URL = "https://drawlyz-v2.herokuapp.com/api/";
//        String BASE_URL = "http://192.168.1.12:3000/api/";
        if (mainRetrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();
            mainRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mainRetrofit;
    }

    public static Retrofit getPuppeteerRetrofit() {
        String BASE_URL = "https://drawlyz-v2-puppeteer.herokuapp.com/api/";
//        String BASE_URL = "http://192.168.1.12:5000/api/";
        if (puppeteerRetrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .build();
            puppeteerRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return puppeteerRetrofit;
    }

}
