package com.mujadid.spkpentablet.api;

import androidx.annotation.Nullable;

import com.mujadid.spkpentablet.models.Criteria;
import com.mujadid.spkpentablet.models.Search;
import com.mujadid.spkpentablet.models.Tablet;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("tablets")
    Call<List<Tablet>> getTablets(@Nullable @Query("brand") String brand);

    @GET("brands")
    Call<List<String>> getBrands();

    @GET("criterias")
    Call<List<Criteria>> getCriterias();

    @POST("scores")
    Call<List<Tablet>> getScores(@Body List<Criteria> criterias);

    @GET("search")
    Call<List<Search>> getSearch(@Query("keywords") String keywords);
}
