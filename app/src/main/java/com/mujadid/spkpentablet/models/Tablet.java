package com.mujadid.spkpentablet.models;

import java.io.Serializable;
import java.util.List;

public class Tablet implements Serializable {
    public String _id;
    public String type;
    public String brand;
    public String model;
    public String image;
    public String url;
    public float price;
    public float priceIDR;
    public float dimensionX;
    public float dimensionY;
    public float dimensionZ;
    public float resolutionX;
    public float resolutionY;
    public float workingAreaX;
    public float workingAreaY;
    public float workingAreaDiagonalInch;
    public int pressureLevels;
    public int totalKeys;
    public int radialKey;
    public int tiltRecognitionSupport;
    public String featuredControls;
    public float fireScore;
    public List<Criteria> criterias;

    public static class Criteria implements Serializable {
        public String name;
        public float groupScore;
        public List<Category> categories;
    }

    public static class Category implements Serializable {
        public String name;
        public float score;
    }
}