package com.mujadid.spkpentablet.models;

import java.io.Serializable;
import java.util.List;

public class Criteria implements Serializable {
    public String name;
    public List<String> categories;
}
