package com.mujadid.spkpentablet.models;

import java.io.Serializable;
import java.util.List;

public class Search implements Serializable {
    public String name;
    public String image;
    public String url;
    public List<Result> data;

    public static class Result implements Serializable {
        public String name;
        public String image;
        public String url;
        public String price;
    }
}