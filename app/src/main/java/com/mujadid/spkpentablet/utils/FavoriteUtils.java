package com.mujadid.spkpentablet.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mujadid.spkpentablet.models.Tablet;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;

public class FavoriteUtils {
    public static ArrayList<Tablet> loadFavorites(Context context) {
        ArrayList<Tablet> pentablets = new ArrayList<>();
        try {
            InputStream inputStream = context.openFileInput("favorites.json");
            Reader reader = new InputStreamReader(inputStream);
            Gson gson = new Gson();
            pentablets = gson.fromJson(reader, new TypeToken<ArrayList<Tablet>>() {
            }.getType());
        } catch (Exception e) {
            Log.e("JSON", "Failed to load data: " + e.getMessage());
        }
        return pentablets;
    }

    public static void writeData(Context context, Tablet pentablet) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<Tablet> pentablets = FavoriteUtils.loadFavorites(context);
        pentablet.criterias = null;
        pentablet.fireScore = 0;
        pentablets.add(pentablet);
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("favorites.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(gson.toJson(pentablets));
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("JSON", "File write failed: " + e.toString());
        }
    }

    public static boolean isFavorited(Context context, String _id) {
        ArrayList<Tablet> pentablets = FavoriteUtils.loadFavorites(context);
        for (int i = 0; i < pentablets.size(); i++) {
            if (pentablets.get(i)._id.equals(_id)) {
                return true;
            }
        }
        return false;
    }

    public static void removeWithId(Context context, String _id) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<Tablet> pentablets = FavoriteUtils.loadFavorites(context);
        for (int i = 0; i < pentablets.size(); i++) {
            if (pentablets.get(i)._id.equals(_id)) {
                pentablets.remove(i);
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("favorites.json", Context.MODE_PRIVATE));
                    outputStreamWriter.write(gson.toJson(pentablets));
                    outputStreamWriter.close();
                } catch (IOException e) {
                    Log.e("JSON", "File write failed: " + e.toString());
                }
                break;
            }
        }
    }
}
