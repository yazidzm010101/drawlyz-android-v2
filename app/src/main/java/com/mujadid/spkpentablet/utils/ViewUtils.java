package com.mujadid.spkpentablet.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.TypedValue;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

public class ViewUtils {

    public static int dpToPx(Context context, int dp) {
        return (int) (dp * (context.getResources().getDisplayMetrics().density));
    }

    public static int pxToDp(Context context, int px) {
        return (int) (px / (context.getResources().getDisplayMetrics().density));
    }

    public static Drawable tintDrawable(Drawable drawable, int color) {
        Drawable drawableCompat = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawableCompat, color);
        return drawableCompat;
    }

    public static int getAttributeColor(Context context, int attributeId) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attributeId, typedValue, true);
        int color = ContextCompat.getColor(context, typedValue.resourceId);
        return color;
    }


    public static SpannableString drawableToString(String string, String target, Drawable drawable) {
        SpannableString spannableString = new SpannableString(string);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM);
        int index = string.indexOf(target);
        int length = target.length();
        spannableString.setSpan(span, index, index + length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
}
