package com.mujadid.spkpentablet.activities;

import android.os.Bundle;

import com.mujadid.spkpentablet.R;

public class AboutActivity extends SubActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addView(R.layout.submenu_about);
        setupToolbar(getString(R.string.title_about));
    }

}