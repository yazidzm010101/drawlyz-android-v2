package com.mujadid.spkpentablet.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.IntroductionAdapter;

public class IntroActivity extends AppCompatActivity {
    ExtendedFloatingActionButton exfabFinish;
    ViewPager viewPager;
    IntroductionAdapter introductionAdapter;
    TabLayout indicators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduction);
        setupViews();
    }

    protected void setupViews() {
        viewPager = findViewById(R.id.view_pager);
        indicators = findViewById(R.id.indicators);
        exfabFinish = findViewById(R.id.exfab_finish);
        introductionAdapter = new IntroductionAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(introductionAdapter);
        indicators.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int lastIndex = introductionAdapter.getCount() - 1;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == lastIndex) {
                    exfabFinish.setVisibility(ViewPager.VISIBLE);
                    return;
                }
                exfabFinish.setVisibility(ViewPager.INVISIBLE);

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void finishIntroduction(View view) {
        SharedPreferences pref = getSharedPreferences("MyPref", 0);
        pref.edit().putBoolean("isIntroduction", false).commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(intent);
        finish();
    }
}