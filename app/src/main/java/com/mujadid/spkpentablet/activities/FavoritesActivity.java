package com.mujadid.spkpentablet.activities;

import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.FavoritesFragment;

public class FavoritesActivity extends SubActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(getString(R.string.title_favorites));
        setupFragments();
    }

    void setupFragments() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        FavoritesFragment favoritesFragment = new FavoritesFragment();
        transaction.replace(R.id.view_content, favoritesFragment);
        transaction.commit();
    }

}