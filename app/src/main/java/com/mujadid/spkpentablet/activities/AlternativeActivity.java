package com.mujadid.spkpentablet.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.widget.HorizontalScrollView;

import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.TabletsScoreFragment;
import com.mujadid.spkpentablet.models.Criteria;

import java.util.ArrayList;

public class AlternativeActivity extends SubActivity {
    private ArrayList<Criteria> criterias;
    private TabletsScoreFragment pentabletsScoreFragment;
    private HorizontalScrollView viewChips;
    private ChipGroup chipGroup;
    private AppBarLayout mainAppbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        criterias = (ArrayList<Criteria>) getIntent().getSerializableExtra("criterias");
        setupToolbar(getString(R.string.title_result));
        setChipGroups();
        setupFragments();
    }

    void setChipGroups() {
        mainAppbar = findViewById(R.id.main_appbar);
        viewChips = (HorizontalScrollView) getLayoutInflater().inflate(R.layout.group_chip, null);
        chipGroup = viewChips.findViewById(R.id.chip_group);
        for (int i = 0; i < criterias.size(); i++) {
            Chip chip = new Chip(new ContextThemeWrapper(this, R.style.Widget_MaterialComponents_Chip_Choice));
            String name = criterias.get(i).name + ": " + TextUtils.join(", ", criterias.get(i).categories);
            chip.setText(name);
            chipGroup.addView(chip);
        }

        mainAppbar.addView(viewChips);
    }

    void setupFragments() {
        pentabletsScoreFragment = TabletsScoreFragment.newInstance(criterias);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.view_content, pentabletsScoreFragment);
        transaction.commit();
    }

}