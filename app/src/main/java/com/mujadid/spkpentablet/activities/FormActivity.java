package com.mujadid.spkpentablet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.appbar.MaterialToolbar;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.CriteriasFragment;

public class FormActivity extends SubActivity {
    private CriteriasFragment criteriaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(getString(R.string.title_criterias), R.menu.appbar_criteria, onMenuItemClickListener);
        setupFragments();
    }

    void setupFragments() {
        criteriaFragment = new CriteriasFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.view_content, criteriaFragment);
        transaction.commit();
    }

    MaterialToolbar.OnMenuItemClickListener onMenuItemClickListener = new MaterialToolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getItemId() == R.id.action_criteria) {
                if (criteriaFragment.getSelectedCriterias().size() > 0) {
                    Intent intent = new Intent(getApplicationContext(), AlternativeActivity.class);
                    intent.putExtra("criterias", criteriaFragment.getSelectedCriterias());
                    startActivity(intent);
                    return true;
                }
                Toast.makeText(getApplicationContext(), "Select at least one criteria", Toast.LENGTH_SHORT).show();
                return true;
            }
            return false;
        }
    };
}