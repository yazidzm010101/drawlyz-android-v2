package com.mujadid.spkpentablet.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.FuzzyScoresFragment;
import com.mujadid.spkpentablet.fragments.SearchFragment;
import com.mujadid.spkpentablet.fragments.SpecificationsFragment;
import com.mujadid.spkpentablet.utils.ViewUtils;
import com.mujadid.spkpentablet.utils.FavoriteUtils;
import com.mujadid.spkpentablet.models.Tablet;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class TabletActivity extends SubActivity {
    private Tablet pentablet;
    private TabLayout tabLayout;
    private ImageView imImage;
    private ArrayList<Fragment> fragments;
    private LinearLayout body;
    private TextView price;
    private TextView priceIDR;
    private boolean isFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addView(R.layout.submenu_tablet);

        body = findViewById(R.id.body);
        pentablet = (Tablet) getIntent().getSerializableExtra("tablet");

        setupToolbar(pentablet.brand + " " + pentablet.model, R.menu.appbar_pentablet, menuItemClickListener);
        checkFavorite();
        setupHeader();
        setupFragments();
    }

    void setupHeader() {
        DecimalFormat df = new DecimalFormat("###,###,##0.00");
        DecimalFormat dfIDR = new DecimalFormat("###,###,##0");
        price = findViewById(R.id.price);
        priceIDR = findViewById(R.id.priceIDR);

        imImage = findViewById(R.id.image);
        Glide.with(this)
                .load(pentablet.image)
                .into(imImage);
        price.setText("USD ".concat(df.format(pentablet.price)));
        priceIDR.setText("IDR ".concat(dfIDR.format((pentablet.priceIDR))));
    }

    void setupFragments() {
        tabLayout = (TabLayout) findViewById(R.id.tab_group);
        fragments = new ArrayList<>();
        tabLayout.addTab(tabLayout.newTab().setText(R.string.header_spec).setIcon(R.drawable.ic_baseline_list_fill_24));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.header_marketplace).setIcon(R.drawable.ic_baseline_marketplace_fill_24));
        fragments.add(SpecificationsFragment.newInstance(pentablet));
        fragments.add(SearchFragment.newInstance(pentablet.brand + " " + pentablet.model));

        if (pentablet.criterias != null) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.header_score).setIcon(R.drawable.ic_baseline_filter_24));
            fragments.add(FuzzyScoresFragment.newInstance(pentablet.criterias, pentablet.fireScore));
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        for (int i = 0; i < fragments.size(); i++) {
            if (i == 0) {
                fragmentTransaction.replace(body.getId(), fragments.get(i));
            } else {
                fragmentTransaction.add(body.getId(), fragments.get(i));
            }
            if (i > 0)
                fragmentTransaction.hide(fragments.get(i));
        }
        fragmentTransaction.commit();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.show(fragments.get(position));
                fragmentTransaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.hide(fragments.get(position));
                fragmentTransaction.commit();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    void checkFavorite() {
        String _id = pentablet._id;
        MenuItem item = mainToolbar.getMenu().findItem(R.id.action_favorite);

        isFavorite = FavoriteUtils.isFavorited(getApplicationContext(), _id);
        String title = isFavorite ? getString(R.string.action_unfavorite) : getString(R.string.action_favorite);
        Drawable drawable = isFavorite ?
                ViewUtils.tintDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_heart_fill_24), getResources().getColor(R.color.red_700)) :
                ViewUtils.tintDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_heart_24), ViewUtils.getAttributeColor(this, R.attr.colorControlNormal));

        item.setTitle(title);
        item.setIcon(drawable);
    }


    Toolbar.OnMenuItemClickListener menuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            String toastMessage;
            if (item.getItemId() == R.id.action_favorite) {
                if (isFavorite) {
                    FavoriteUtils.removeWithId(getApplicationContext(), pentablet._id);
                    toastMessage = getString(R.string.message_unfavorite);
                } else {
                    FavoriteUtils.writeData(getApplicationContext(), pentablet);
                    toastMessage = getString(R.string.message_favorite);
                }
                Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_SHORT).show();

                checkFavorite();
                return true;
            } else if (item.getItemId() == R.id.action_openweb) {
                String url = pentablet.url;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (browserIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(browserIntent);
                else
                    Toast.makeText(getApplicationContext(),
                            R.string.message_nobrowser
                            , Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    };


}