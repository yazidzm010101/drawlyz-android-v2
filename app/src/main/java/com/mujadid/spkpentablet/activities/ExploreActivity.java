package com.mujadid.spkpentablet.activities;

import android.os.Bundle;
import android.widget.HorizontalScrollView;

import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.fragments.TabletsFragment;

import java.util.ArrayList;
import java.util.List;

public class ExploreActivity extends SubActivity {
    private AppBarLayout mainAppbar;
    private TabLayout tabLayout;
    private ArrayList<TabletsFragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(getString(R.string.title_explore));
        setupTabs();
    }


    void setupTabs() {
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) getLayoutInflater().inflate(R.layout.group_tab, null);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        TabletsFragment fragment = new TabletsFragment();

        fragments = new ArrayList<>();
        mainAppbar = findViewById(R.id.main_appbar);
        tabLayout = (TabLayout) horizontalScrollView.findViewById(R.id.tab_group);

        tabLayout.addTab(tabLayout.newTab().setText("All"));
        mainAppbar.addView(horizontalScrollView);

        fragments.add(fragment);
        fragmentTransaction.add(viewContent.getId(), fragment);
        fragmentTransaction.commit();

        fragment.brandCallback = new TabletsFragment.BrandCallback() {
            @Override
            public void execute(List<String> brands) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                for (int i = 0; i < brands.size(); i++) {
                    TabletsFragment fragment = TabletsFragment.newInstance(brands.get(i));
                    fragments.add(fragment);
                    fragmentTransaction.add(viewContent.getId(), fragment);
                    fragmentTransaction.hide(fragment);
                    tabLayout.addTab(tabLayout.newTab().setText(brands.get(i)));
                }
                fragmentTransaction.commit();
            }
        };

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.show(fragments.get(position));
                fragmentTransaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.hide(fragments.get(position));
                fragmentTransaction.commit();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}