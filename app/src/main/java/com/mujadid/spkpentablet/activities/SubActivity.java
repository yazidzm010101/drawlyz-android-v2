package com.mujadid.spkpentablet.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.MenuRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.MaterialToolbar;
import com.mujadid.spkpentablet.R;

public class SubActivity extends AppCompatActivity {
    MaterialToolbar mainToolbar;
    LinearLayout viewContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.submenu);
        mainToolbar = findViewById(R.id.main_toolbar);
        viewContent = findViewById(R.id.view_content);
        mainToolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_left_24);
        mainToolbar.setNavigationOnClickListener(onNavigationItemSelectedListener);
    }

    protected View.OnClickListener onNavigationItemSelectedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    protected View addView(@LayoutRes int resId) {
        View view = getLayoutInflater().inflate(resId, null);
        viewContent.addView(view);
        return view;
    }

    protected void setupToolbar(String title) {
        mainToolbar.setTitle(title);
    }

    public void setupToolbar(String title,
                             @MenuRes int menu,
                             Toolbar.OnMenuItemClickListener menuListener) {
        setupToolbar(title);
        mainToolbar.getMenu().clear();
        mainToolbar.inflateMenu(menu);
        mainToolbar.setOnMenuItemClickListener(menuListener);
    }

}