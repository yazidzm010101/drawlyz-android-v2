package com.mujadid.spkpentablet.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mujadid.spkpentablet.R;

import java.util.ArrayList;

public class HelpActivity extends SubActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(getString(R.string.title_help));
        setupViews();
    }

    protected void setupViews() {
        ArrayList<Integer> drawableId = new ArrayList<>();
        String[] titles = getResources().getStringArray(R.array.criteria_help_title);
        String[] bodies = getResources().getStringArray(R.array.criteria_help_body);

        drawableId.add(R.drawable.ic_baseline_aspect_ratio_fill_24);
        drawableId.add(R.drawable.ic_baseline_pressure_fill_24);
        drawableId.add(R.drawable.ic_baseline_command_24);
        drawableId.add(R.drawable.ic_baseline_radial_fill_24);
        drawableId.add(R.drawable.ic_baseline_tilt_fill_24);
        drawableId.add(R.drawable.ic_baseline_currency_dollar_24);

        for (int i = 0; i < titles.length; i++) {
            View view = addView(R.layout.item_help);
            ((FloatingActionButton) view.findViewById(R.id.fab_image)).setImageResource(drawableId.get(i));
            ((TextView) view.findViewById(R.id.header)).setText(titles[i]);
            ((TextView) view.findViewById(R.id.body)).setText(bodies[i]);
        }

    }

}