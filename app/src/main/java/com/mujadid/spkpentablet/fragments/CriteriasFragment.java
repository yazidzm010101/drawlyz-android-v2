package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.CriteriaAdapter;
import com.mujadid.spkpentablet.models.Criteria;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CriteriasFragment extends APIFragment {
    CriteriaAdapter criteriaAdapter;

    public CriteriasFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.submenu_api, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        criteriaAdapter = new CriteriaAdapter(getContext());
        Call<List<Criteria>> call = API.getCriterias();
        call.enqueue(new Callback<List<Criteria>>() {
            @Override
            public void onResponse(Call<List<Criteria>> call, Response<List<Criteria>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        criteriaAdapter.setData(response.body());
                        showData();
                        return;
                    }
                    showNoData();
                    return;
                }
                showFailure();
            }

            @Override
            public void onFailure(Call<List<Criteria>> call, Throwable t) {
                showFailure();
            }
        });
        viewData.setLayoutManager(new LinearLayoutManager(getContext()));
        viewData.setAdapter(criteriaAdapter);
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_filter_24;
    }

    public ArrayList<Criteria> getSelectedCriterias() {
        return criteriaAdapter.getSelectedCriterias();
    }

}