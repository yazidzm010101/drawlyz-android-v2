package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.SpecificationAdapter;
import com.mujadid.spkpentablet.models.Tablet;

import java.text.DecimalFormat;

public class SpecificationsFragment extends APIFragment {
    private static final String ARG_TABLET = "tablet";
    Tablet tablet;

    public SpecificationsFragment() {

    }

    public static SpecificationsFragment newInstance(Tablet tablet) {
        Bundle args = new Bundle();
        SpecificationsFragment fragment = new SpecificationsFragment();
        args.putSerializable(ARG_TABLET, tablet);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.tablet = (Tablet)getArguments().getSerializable(ARG_TABLET);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        DecimalFormat dF = new DecimalFormat("0.00");
        String strBrand = tablet.brand;
        String strModel = tablet.model;
        String strType = tablet.type;
        String strResolution = tablet.resolutionX != 0 && tablet.resolutionY != 0 ?
                tablet.resolutionX + " x " + tablet.resolutionY : null;
        String strDimension
                = tablet.dimensionX + " x "
                + tablet.dimensionY + " x "
                + tablet.dimensionZ + " mm";
        String strWorkingArea
                = tablet.workingAreaX + " x "
                + tablet.workingAreaY + " mm\n"
                + dF.format(tablet.workingAreaX / 25.4f) + " x "
                + dF.format(tablet.workingAreaY / 25.4f) + " inch\n"
                + tablet.workingAreaDiagonalInch + "\" (Diagonally in inch)";
        String strPressureLv = tablet.pressureLevels + " Lv";
        String strFeaturedControls = tablet.featuredControls.replace("+", "\n");

        SpecificationAdapter adSpecs = new SpecificationAdapter();

        viewData.setLayoutManager(new LinearLayoutManager(getContext()));
        viewData.setFocusable(false);
        adSpecs.addItem(getString(R.string.spec_type), strType);
        adSpecs.addItem(getString(R.string.spec_brand), strBrand);
        adSpecs.addItem(getString(R.string.spec_model), strModel);
        adSpecs.addItem(getString(R.string.spec_dimension), strDimension);
        adSpecs.addItem(getString(R.string.spec_working_area), strWorkingArea);
        if (strResolution != null)
            adSpecs.addItem(getString(R.string.spec_resolution), strResolution);
        adSpecs.addItem(getString(R.string.spec_pressure_level), strPressureLv);
        adSpecs.addItem(getString(R.string.spec_others), strFeaturedControls);

        viewData.setAdapter(adSpecs);
        viewData.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        showData();
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_tablet_landscape_fill_24;
    }


}