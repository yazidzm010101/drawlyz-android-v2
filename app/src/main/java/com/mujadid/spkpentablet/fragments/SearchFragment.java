package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.SearchAdapter;
import com.mujadid.spkpentablet.api.APIInterface;
import com.mujadid.spkpentablet.api.APIRetrofit;
import com.mujadid.spkpentablet.models.Search;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends APIFragment {
    private static final String ARG_KEYWORDS = "keywords";
    String keywords;

    public SearchFragment() {

    }

    public static SearchFragment newInstance(String keywords) {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        args.putString(ARG_KEYWORDS, keywords);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.keywords = getArguments().getString(ARG_KEYWORDS);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.retrofit = APIRetrofit.getPuppeteerRetrofit();
        this.API = retrofit.create(APIInterface.class);
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        SearchAdapter searchAdapter = new SearchAdapter(getContext());
        Call<List<Search>> call = API.getSearch(keywords);
        call.enqueue(new Callback<List<Search>>() {
            @Override
            public void onResponse(Call<List<Search>> call, Response<List<Search>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        searchAdapter.setData(response.body());
                        showData();
                        return;
                    }
                    showNoData();
                    return;
                }
                showFailure();
            }

            @Override
            public void onFailure(Call<List<Search>> call, Throwable t) {
                showFailure();
            }
        });

        viewData.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        viewData.setAdapter(searchAdapter);
        viewData.setPadding(0, 0, 0, 0);
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_tablet_landscape_fill_24;
    }


}