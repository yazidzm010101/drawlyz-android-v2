package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.TabletAdapter;
import com.mujadid.spkpentablet.models.Criteria;
import com.mujadid.spkpentablet.models.Tablet;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabletsScoreFragment extends APIFragment {
    private static final String ARG_CRITERIAS = "criterias";
    List<Criteria> criterias;

    TabletsScoreFragment() {

    }

    public static TabletsScoreFragment newInstance(ArrayList<Criteria> criterias) {
        TabletsScoreFragment fragment = new TabletsScoreFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRITERIAS, criterias);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            criterias = (List<Criteria>) getArguments().getSerializable(ARG_CRITERIAS);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView noData = view.findViewById(R.id.nodata_message);
        noData.setText(R.string.info_nodata_recommendation);
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        TabletAdapter tabletAdapter = new TabletAdapter(getContext());
        Call<List<Tablet>> call = API.getScores(criterias);
        call.enqueue(new Callback<List<Tablet>>() {
            @Override
            public void onResponse(Call<List<Tablet>> call, Response<List<Tablet>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        tabletAdapter.setData(response.body());
                        showData();
                        return;
                    }
                    showNoData();
                    return;
                }
                showFailure();
            }

            @Override
            public void onFailure(Call<List<Tablet>> call, Throwable t) {
                showFailure();
            }
        });
        viewData.setLayoutManager(new LinearLayoutManager(getContext()));
        viewData.setAdapter(tabletAdapter);
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_filter_24;
    }

}