package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.TabletAdapter;
import com.mujadid.spkpentablet.models.Tablet;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabletsFragment extends APIFragment {
    private static final String ARG_BRAND = "brand";
    public BrandCallback brandCallback;
    TabletAdapter tabletAdapter;
    String brand;

    public TabletsFragment() {

    }

    public static TabletsFragment newInstance(String brand) {
        TabletsFragment fragment = new TabletsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_BRAND, brand);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.brand = getArguments().getString(ARG_BRAND);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabletAdapter = new TabletAdapter(getContext());
        viewData.setLayoutManager(new LinearLayoutManager(getContext()));
        viewData.setAdapter(tabletAdapter);
        loadData();
    }

    @Override
    public void loadData() {
        if (brandCallback != null) {
            loadBrandAndPentablet();
            return;
        }
        loadPentablet();
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_tablet_landscape_fill_24;
    }

    public interface BrandCallback {
        void execute(List<String> brands);
    }

    public void loadBrandAndPentablet() {
        showLoading();
        Call<List<String>> call = API.getBrands();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        brandCallback.execute(response.body());
                        loadPentablet();
                        return;
                    }
                    showNoData();
                    return;
                }
                showFailure();
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                showFailure();
            }
        });
    }

    public void loadPentablet() {
        showLoading();
        Call<List<Tablet>> call = API.getTablets(this.brand);
        call.enqueue(new Callback<List<Tablet>>() {
            @Override
            public void onResponse(Call<List<Tablet>> call, Response<List<Tablet>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        tabletAdapter.setData(response.body());
                        showData();
                        return;
                    }
                    showNoData();
                    return;
                }
                showFailure();
            }

            @Override
            public void onFailure(Call<List<Tablet>> call, Throwable t) {
                showFailure();
            }
        });
    }

}