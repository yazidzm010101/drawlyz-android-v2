package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.ScoreAdapter;
import com.mujadid.spkpentablet.models.Tablet;

import java.util.ArrayList;
import java.util.List;

public class FuzzyScoresFragment extends APIFragment {
    private static final String ARG_CRITERIAS = "criterias";
    private static final String ARG_FIRESCORE = "firescore";

    ArrayList<Tablet.Criteria> criterias;
    float fireScore;

    public FuzzyScoresFragment() {

    }

    public static FuzzyScoresFragment newInstance(List<Tablet.Criteria> criterias, float fireScore) {
        Bundle args = new Bundle();
        FuzzyScoresFragment fragment = new FuzzyScoresFragment();
        args.putSerializable(ARG_CRITERIAS, new ArrayList<>(criterias));
        args.putFloat(ARG_FIRESCORE, fireScore);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.criterias = (ArrayList<Tablet.Criteria>) getArguments().getSerializable(ARG_CRITERIAS);
            this.fireScore = getArguments().getFloat(ARG_FIRESCORE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        ScoreAdapter scoresAdapter = new ScoreAdapter();
        scoresAdapter.setData(criterias, fireScore);
        viewData.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        viewData.setAdapter(scoresAdapter);
        showData();
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_tablet_landscape_fill_24;
    }


}