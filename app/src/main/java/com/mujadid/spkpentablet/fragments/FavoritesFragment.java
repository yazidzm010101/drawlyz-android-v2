package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.adapters.TabletAdapter;
import com.mujadid.spkpentablet.utils.FavoriteUtils;
import com.mujadid.spkpentablet.models.Tablet;
import com.mujadid.spkpentablet.utils.ViewUtils;

import java.util.ArrayList;

public class FavoritesFragment extends APIFragment {

    public FavoritesFragment() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView noData = view.findViewById(R.id.nodata_message);
        SpannableString noDataMessage = ViewUtils.drawableToString(
                getString(R.string.info_nodata_favorite),
                "%s",
                ContextCompat.getDrawable(getContext(), R.drawable.ic_baseline_heart_24)
        );
        noData.setText(noDataMessage);
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void loadData() {
        showLoading();
        ArrayList<Tablet> tablets = FavoriteUtils.loadFavorites(getContext());
        if (tablets.size() == 0) {
            showNoData();
            return;
        }
        ;
        TabletAdapter tabletAdapter = new TabletAdapter(getContext());
        tabletAdapter.setData(tablets);
        viewData.setLayoutManager(new LinearLayoutManager(getContext()));
        viewData.setAdapter(tabletAdapter);
        showData();
    }

    @Override
    public @DrawableRes
    int getIcon() {
        return R.drawable.ic_baseline_heart_fill_24;
    }


}