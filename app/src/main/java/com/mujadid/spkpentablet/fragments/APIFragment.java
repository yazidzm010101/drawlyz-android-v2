package com.mujadid.spkpentablet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.mujadid.spkpentablet.R;
import com.mujadid.spkpentablet.api.APIInterface;
import com.mujadid.spkpentablet.api.APIRetrofit;

import retrofit2.Retrofit;

public abstract class APIFragment extends Fragment {
    protected RecyclerView viewData;
    protected LinearLayout viewLoading;
    protected LinearLayout viewFailure;
    protected LinearLayout viewNoData;
    protected Button btnReload;
    protected ImageView icon;
    protected Retrofit retrofit;
    protected APIInterface API;

    protected abstract void loadData();

    protected abstract @DrawableRes
    int getIcon();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.submenu_api, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewData = view.findViewById(R.id.view_data);
        viewFailure = view.findViewById(R.id.view_failure);
        viewLoading = view.findViewById(R.id.view_loading);
        viewNoData = view.findViewById(R.id.view_nodata);
        retrofit = APIRetrofit.getMainRetrofit();
        btnReload = view.findViewById(R.id.reload);
        icon = view.findViewById(R.id.icon);
        API = retrofit.create(APIInterface.class);
        viewData.setFocusable(false);
        setupReload();
        setupIcon();
    }

    protected void showData() {
        viewFailure.setVisibility(View.GONE);
        viewLoading.setVisibility(View.GONE);
        viewNoData.setVisibility(View.GONE);

        viewData.setVisibility(View.VISIBLE);
    }

    protected void showNoData() {
        viewFailure.setVisibility(View.GONE);
        viewLoading.setVisibility(View.GONE);
        viewData.setVisibility(View.GONE);

        viewNoData.setVisibility(View.VISIBLE);
    }

    protected void showFailure() {
        viewLoading.setVisibility(View.GONE);
        viewData.setVisibility(View.GONE);
        viewNoData.setVisibility(View.GONE);

        viewFailure.setVisibility(View.VISIBLE);
    }

    protected void showLoading() {
        viewFailure.setVisibility(View.GONE);
        viewData.setVisibility(View.GONE);
        viewNoData.setVisibility(View.GONE);

        viewLoading.setVisibility(View.VISIBLE);
    }

    protected void setupReload() {
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    protected void setupIcon() {
        icon.setImageDrawable(getContext().getDrawable(getIcon()));
    }
}